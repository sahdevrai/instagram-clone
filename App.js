import React,{useState,useEffect} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View,Button } from 'react-native';
import Landing from './components/auth/Landing';
import firebase from 'firebase/compat';
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import {Provider} from 'react-redux'
import { legacy_createStore as createStore, applyMiddleware} from 'redux'
import rootReducer from './redux/reducers'
import thunk from 'redux-thunk';
import MainScreen from './components/Main'
import add from './components/main/add/add';
import Save from './components/main/add/Save';
import comment from './components/main/feed/comment';
import Likes from './components/main/feed/likes';



    
const store=createStore(rootReducer, applyMiddleware(thunk))

const Stack = createStackNavigator()

export default function App() {

  const [loaded, setLoaded] = useState(false)
  const [loggedin, setLoggedin] = useState(false)


  useEffect(() => {
    firebase.auth().onAuthStateChanged((user)=>{
        if(!user)
        {
          setLoggedin(false);
          setLoaded(true)
        }
        else
        {
          setLoggedin(true); 
          setLoaded(true)
        }
    })
  }, [])
  
if(!loaded)
{
  return(
    <View style={{flex:1,justifyContent:'center',alignItems:"center"}}>
      <Text>
        Loading...
      </Text>
    </View>
  ) 
}

if(!loggedin){
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Landing">
        <Stack.Screen name="Landing" component={Landing} options={{headerShown:true}}/>
        <Stack.Screen name="Login" component={Login} options={{headerShown:true}}/>
        <Stack.Screen name="Register" component={Register} options={{headerShown:true}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

return (
  <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Main">
        <Stack.Screen name="Main" component={MainScreen} options={{headerShown:false}}/>
        <Stack.Screen name="add" component={add} navigation={navigation}/>
        <Stack.Screen name="Save" component={Save} navigation={navigation}/>
        <Stack.Screen name="Comment" component={comment} navigation={navigation}/>
        <Stack.Screen name="Likes" component={Likes} navigation={navigation}/>
        
      </Stack.Navigator>
    </NavigationContainer>      
  </Provider>
  // <Provider store={store}> 
  //   <NavigationContainer >
  //     <Stack.Navigator initialRouteName="Main">
  //       <Stack.Screen key={Date.now()} name="Main" component={MainScreen} navigation={this.props.navigation} options={({ route }) => {
  //         const routeName = getFocusedRouteNameFromRoute(route) ?? 'Feed';

  //         switch (routeName) {
  //           case 'Camera': {
  //             return {
  //               headerTitle: 'Camera',
  //             };
  //           }
  //           case 'chat': {
  //             return {
  //               headerTitle: 'Chat',
  //             };
  //           }
  //           case 'Profile': {
  //             return {
  //               headerTitle: 'Profile',
  //             };
  //           }
  //           case 'Search': {
  //             return {
  //               headerTitle: 'Search',
  //             };
  //           }
  //           case 'Feed':
  //           default: {
  //             return {
  //               headerTitle: 'Instagram',
  //             };
  //           }
  //         }
  //       }}
  //       />
  //       <Stack.Screen key={Date.now()} name="Save" component={SaveScreen} navigation={this.props.navigation} />
  //       <Stack.Screen key={Date.now()} name="video" component={SaveScreen} navigation={this.props.navigation} />
  //       <Stack.Screen key={Date.now()} name="Post" component={PostScreen} navigation={this.props.navigation} />
  //       <Stack.Screen key={Date.now()} name="Chat" component={ChatScreen} navigation={this.props.navigation} />
  //       <Stack.Screen key={Date.now()} name="ChatList" component={ChatListScreen} navigation={this.props.navigation} />
  //       <Stack.Screen key={Date.now()} name="Edit" component={EditScreen} navigation={this.props.navigation} />
  //       <Stack.Screen key={Date.now()} name="Profile" component={ProfileScreen} navigation={this.props.navigation} />
  //       <Stack.Screen key={Date.now()} name="Comment" component={CommentScreen} navigation={this.props.navigation} />
  //       <Stack.Screen key={Date.now()} name="ProfileOther" component={ProfileScreen} navigation={this.props.navigation} />
  //       <Stack.Screen key={Date.now()} name="Blocked" component={BlockedScreen} navigation={this.props.navigation} options={{ headerShown: false }} />
  //     </Stack.Navigator>
  //   </NavigationContainer>
  // </Provider>
)

}
