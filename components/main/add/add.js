import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity,Button, Image } from 'react-native';
import { Camera, CameraType } from 'expo-camera';
import * as ImagePicker from 'expo-image-picker';

export default function Add({navigation}) {
  const [gallerypermission, setgallerypermission] = useState(null);
  const [camerapermission, setcamerapermission] = useState(null);

  const [camera, setcamera] = useState(null);
  const [image, setimage] = useState(null);

  const [type, setType] = useState(CameraType.back);

  useEffect(() => {
    (async () => {
      const cameraStatus = await Camera.requestCameraPermissionsAsync();
      setcamerapermission(cameraStatus.status === 'granted');


      const galleryStatus = await ImagePicker.getMediaLibraryPermissionsAsync()
      setgallerypermission(galleryStatus.status === 'granted');
    })();
  }, []);

  const takePicture= async()=>{
    if(camera)
    {
        console.log("qwerty")
        const data=await camera.takePictureAsync(null);
        setimage(data.uri)
    }
  }

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1,
    });
    setimage(result.uri)

  }


  if (camerapermission === null || gallerypermission===null) {
    return <View />;
  }
  if (camerapermission === false || gallerypermission=== false ) {
    return <Text>No access to camera</Text>;
  }
  return (
    <View style={styles.container}>
      
        <View style={styles.cameraContainer}>
        <Camera ref={ref=>setcamera(ref)} style={styles.fixedRatio} type={type} ratio={'1:1'}/>
        </View>
        <Button
            style={styles.button}
            title="Flip Image"
            onPress={() => {
              setType(type === CameraType.back ? CameraType.front : CameraType.back);
            }}>
            <Text style={styles.text}> Flip </Text>
          </Button>
          <Button title='Take Picture' onPress={()=>takePicture()}></Button>
          <Button title='Pick Image' onPress={()=>pickImage()}></Button>
          <Button title='Save' onPress={()=>navigation.navigate('Save', {image})}></Button>

          {image && <Image source={{uri:image}} style={{flex:1}}/>}
      
    </View>
  );
}

const styles = StyleSheet.create({ 
    container:{
        flex:1
    },
    cameraContainer:{
        flex:1,
        flexDirection:'row'
    },
    fixedRatio:{
        flex:1,
        aspectRatio:1
    },
    button:{
        flex:.1,
        alignSelf:'flex-end',
        alignItems:'center' 
    },
    text:{

    }
}); 
