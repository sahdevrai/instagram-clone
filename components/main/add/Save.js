import React,{useState} from 'react'
import { View, Text,Image, TextInput,Button } from 'react-native'
import firebase from 'firebase/compat'
import '../../../server/firebase'
import { NavigationContainer } from '@react-navigation/native'


function Save(props) {
     const img=props.route.params.image
     console.log(img)
    const [caption, setCaption] = useState("")



    const uploadImage= async()=>{
        const uri=props.route.params.image;
        console.log(uri)
    const path=`post/${firebase.auth().currentUser.uid}/${Date.now()}`  
    console.log(path)

        const response =await fetch(uri)
        const blob=await response.blob();
        const task = firebase
        .storage()
        .ref()
        .child(path)
        .put(blob); 

        const taskprog=snapshot=>{
            console.log(`transferred: ${snapshot.bytesTransferred}`)
        }

        const taskcompleted=()=>{
            task.snapshot.ref.getDownloadURL().then((snapshot)=>{
                savePostData(snapshot);
                console.log(snapshot)
            })
        }

        const taskerr=snapshot=>{
            console.log(snapshot)
        }

        task.on("state_changed",taskprog,taskerr,taskcompleted)

       
    }


    const uploadStory= async()=>{
        const uri=props.route.params.image;
        console.log(uri)
    const path=`story/${firebase.auth().currentUser.uid}/${Date.now()}`  
    console.log(path)

        const response =await fetch(uri)
        const blob=await response.blob();
        const task = firebase
        .storage()
        .ref()
        .child(path)
        .put(blob); 

        const taskprog=snapshot=>{
            console.log(`transferred: ${snapshot.bytesTransferred}`)
        }

        const taskcompleted=()=>{
            task.snapshot.ref.getDownloadURL().then((snapshot)=>{
                saveStory(snapshot);
                console.log(snapshot)
            })
        }

        const taskerr=snapshot=>{
            console.log(snapshot)
        }

        task.on("state_changed",taskprog,taskerr,taskcompleted)

       
    }


    const saveStory = (downloadURL) => {

        firebase.firestore()
            .collection('posts')
            .doc(firebase.auth().currentUser.uid)
            .collection("userStory")
            .add({
                downloadURL,
                caption,
                creation: firebase.firestore.FieldValue.serverTimestamp()
            }).then((function () {
                props.navigation.popToTop()
            }))
    }


    const savePostData = (downloadURL) => {

        firebase.firestore()
            .collection('posts')
            .doc(firebase.auth().currentUser.uid)
            .collection("userPosts")
            .add({
                downloadURL,
                caption,
                creation: firebase.firestore.FieldValue.serverTimestamp()
            }).then((function () {
                props.navigation.popToTop()
            }))
    }


  return (
    <View style={{flex:1}}>
        <Image style={{marginTop:"20px",flex:1}} source={{uri:img}}/>
        <TextInput placeholder='Write a Caption...' onChangeText={(caption)=>setCaption(caption)}/>
        <Button title="Save" onPress={()=>uploadImage()}/>
        <Button title="Save Story" onPress={()=>uploadStory()}/>

    </View>
  )
}

export default Save