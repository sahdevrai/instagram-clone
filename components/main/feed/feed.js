import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text, Image, FlatList, Button } from 'react-native'
import firebase from 'firebase/compat';
import { connect } from 'react-redux'
import { fetchUsersFollowingPosts } from '../../../redux/actions';

function Feed(props) {
    const [posts, setPosts] = useState([]);
    const [story, setstory] = useState([]);

    console.log('props')
    const [arr, setarr] = useState([]);
  
console.log(props)

useEffect(() => {
  // setarr(fetchUsersFollowingPosts())
    // console.log('arr')
    // console.log(arr)
    const qwert=[]
    console.log('props.feed')

    console.log(props.following)
    console.log(props.feed)

    const array=props.following
    array.forEach(element => {
        const sec=[]
        firebase.firestore().collection("posts")
        .doc(element)
        .collection("userStory")
        .orderBy("creation", "asc")
        .get()
        .then((snapshot) => {
            console.log('-==-=-=-=-w=e-=we-w=e-w=e')

            sec.push(snapshot.docs['0'].ref.path.split('/')[3])
            console.log(snapshot.docs['0'].ref.path.split('/')[3])
        })
        qwert.push(sec)
        console.log('qwert')
        console.log(qwert)
        setstory(qwert)

    });
    setstory(qwert)
    console.log('sfdfdfstory')

    console.log(story)

    // let posts = [];
    console.log('before')
    console.log(props.usersLoaded)
    console.log(props.following.length)
    

  if(props.usersFollowingLoaded == props.following.length && props.following.length !== 0){

    console.log('under')
    props.feed.sort(function (x, y) {
      return x.creation - y.creation;
  }
  )
  setPosts(props.feed);

      console.log("posts")
      console.log(posts)

  }
  console.log('after')

},  [props.usersFollowingLoaded,props.feed])
console.log("posts")
      console.log(posts)
    const onLikePress = (userId, postId) => {
      firebase.firestore()
          .collection("posts")
          .doc(userId)
          .collection("userPosts")
          .doc(postId)
          .collection("likes")
          .doc(firebase.auth().currentUser.uid)
          .set({
            'likes':true
          })
  }
  const onDislikePress = (userId, postId) => {
      firebase.firestore()
          .collection("posts")
          .doc(userId)
          .collection("userPosts")
          .doc(postId)
          .collection("likes")
          .doc(firebase.auth().currentUser.uid)
          .delete()
  }
                      
    return (
      <View style={styles.container}>
          <View style={styles.containerGallery}>
              <FlatList
                  numColumns={1}
                  horizontal={false}
                  data={posts}
                  renderItem={({ item }) => (
                      <View
                          style={styles.containerImage}>
                          <Text style={styles.container}>{item.user.name}</Text>
                          <Image
                              style={styles.image}
                              source={{ uri: item.downloadURL }}
                          />
                          <Text style={{fontSize:"20px"}}>
                              {item.caption}
                              </Text>
                              <Text
                              onPress={() => props.navigation.navigate('Likes', { postId: item.id, uid: item.user.uid })}>
                              View Likes...
                              </Text>
                          { item.currentUserLike ?
                              (
                                  <Button
                                      title="Dislike"
                                      onPress={() => onDislikePress(item.user.uid, item.id)} />
                              )
                              :
                              (
                                  <Button
                                      title="Like"
                                      onPress={() => onLikePress(item.user.uid, item.id)} />
                              )
                          }
                          <Text
                              onPress={() => props.navigation.navigate('Comment', { postId: item.id, uid: item.user.uid })}>
                              View Comments...
                              </Text>
                      </View>

                  )}

              />
          </View>
      </View>

  )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerInfo: {
        margin: 20
    },
    containerGallery: {
        flex: 1
    },
    containerImage: {
        flex: 1 / 3

    },
    image: {
        flex: 1,
        aspectRatio: 1 / 1
    }
})
const mapStateToProps = (store) => ({
    currentUser: store.userState.currentUser,
    following: store.userState.following,
    users: store.usersState.users,
    feed: store.usersState.feed,
    usersFollowingLoaded: store.usersState.usersFollowingLoaded,

})
export default connect(mapStateToProps, null)(Feed);