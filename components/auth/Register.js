import React,{useState} from 'react'
import { StyleSheet, Text, View,TextInput,Button } from 'react-native';
import firebase from 'firebase/compat';
import './../../server/firebase'
import { Snackbar } from 'react-native-paper';



function Register({navigation}) {

    let user={
        name:"",
        username:"",
        email:"",
        password:""
    }

    const [userDetails, setUserDetails] = useState(user);
    const [isValid, setIsValid] = useState(true);

    const register=()=>{
        

        if (userDetails.name.length == 0 || userDetails.username.length == 0 || userDetails.email.length == 0 || userDetails.password.length == 0) {
            setIsValid({ bool: true, boolSnack: true, message: "Please fill out everything" })
            return;
        }
        if (userDetails.password.length < 6) {
            setIsValid({ bool: true, boolSnack: true, message: "passwords must be at least 6 characters" })
            return;
        }
     
console.log(userDetails.username)
        firebase.firestore()
            .collection('users')
            .where('username', '==', userDetails.username)
            .get()
            .then((snapshot) => {
                if (!snapshot.empty) {
                    
                    setIsValid({ bool: true, boolSnack: true, message: "Username already exists" })
                    return
                }
                else if (snapshot.empty) {
                    
                    firebase.auth().createUserWithEmailAndPassword(userDetails.email, userDetails.password)
                        .then(() => {
                            console.log(firebase.auth().currentUser.uid);
                            firebase.firestore().collection("users")
                                .doc(firebase.auth().currentUser.uid)
                                .set({
                                    'name':userDetails.name,
                                    'email':userDetails.email,
                                    'username':userDetails.username,
                                    image: 'default',
                                    followingCount: 0,
                                    followersCount: 0,

                                })
                        })
                        .catch(() => {
                            setIsValid({ bool: true, boolSnack: true, message: "Something went wrong" })
                        })
                }
                
            }).catch(() => {
                setIsValid({ bool: true, boolSnack: true, message: "Something went wrong" })
            })

    }


  return (
      <View style={styles.center}>
    <View style={styles.formCenter}>
    <TextInput
                    style={styles.input}
                    placeholder="Username"
                    keyboardType="twitter"
                    onChangeText={(val) => setUserDetails(userDetails=>({
                        ...userDetails,
                        username:val
                    }))}
                />
        <TextInput 
        style={styles.input}
            placeholder='name'
            onChangeText={(val)=>{setUserDetails(userDetails=>({
                ...userDetails,
                name:val
            }))}}/>
        
        <TextInput 
        style={styles.input}
            placeholder='email'
            onChangeText={(val)=>{setUserDetails(userDetails=>({
                ...userDetails,
                email:val
            }));console.log(userDetails.email," ",val)}}/>
        
        <TextInput 
        style={styles.input}
            placeholder='password'
            secureTextEntry={true}
            onChangeText={(val)=>{setUserDetails(userDetails=>({
                ...userDetails,
                password:val
            })); console.log(userDetails.password," ",val)}}/>
        <Button
            title='Register'
            onPress={()=>register()}/>
    </View>
    <View style={styles.bottomButton} >
                <Text
                    onPress={() =>navigation.navigate("Login")} >
                    Already have an account? SignIn.
                </Text>
            </View>
            <Snackbar
            style={styles.snack}
                visible={isValid.boolSnack}
                duration={2000}
                onDismiss={() => { setIsValid({ boolSnack: false }) }}>
                {isValid.message}
            </Snackbar>
    </View>

  )
}


const styles=StyleSheet.create({
    snack:{
        backgroundColor:"grey",
        backfaceVisibility:"visible"
    },
    input:{
        padding:"10px",
        borderWidth:"1px",borderColor:"silver",
        margin:"1px"
    }
    ,
    formCenter:{
        flex:1,
        alignContent:"center",
        justifyContent:"center",
        marginLeft:"10%",
        marginRight:"10%"
    },
    center:{
        flex:1
    },
    bottomButton: {
        alignContent: 'center',
        borderTopColor: 'gray',
        borderTopWidth: 1,
        padding: 10,
        textAlign: 'center',
    }
})

export default Register