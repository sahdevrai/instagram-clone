import React,{useState} from 'react'
import { StyleSheet, Text, View,TextInput,Button } from 'react-native';
import firebase from 'firebase/compat';
import './../../server/firebase'
import Register from './Register'

function Login({navigation}) {

    let user={
        email:"",
        password:""
    }

    const [userDetails, setUserDetails] = useState(user);

    const login=()=>{
        firebase.auth().signInWithEmailAndPassword(userDetails.email,userDetails.password)
        .then((result)=>{
            console.log(result)
        })
        .catch((error)=>{
            console.log(error)
        })
    }

  return (
    <View style={styles.center}>
    <View style={styles.formCenter}>
        <TextInput 
        style={styles.input}
            placeholder='email'
            onChangeText={(val)=>{setUserDetails(userDetails=>({
                ...userDetails,
                email:val
            }))}}/>
        
        <TextInput 
        style={styles.input}
            placeholder='password'
            secureTextEntry={true}
            onChangeText={(val)=>{setUserDetails(userDetails=>({
                ...userDetails,
                password:val
            }))}}/>
        <Button 
            title='Login'
            onPress={()=>login()}/>
    </View>


    <View style={styles.bottomButton} >
                <Text
                    title="Register"
                    onPress={() => navigation.navigate("Register")} >
                    Don't have an account? SignUp.
                </Text>
            </View>
</View>
    
  )
}

const styles=StyleSheet.create({
    input:{
        padding:"10px",
        borderWidth:"1px",borderColor:"silver",
        margin:"1px"
    },
    formCenter:{
        flex:1,
        alignContent:"center",
        justifyContent:"center",
        marginLeft:"10%",
        marginRight:"10%"
    },
    center:{
        flex:1
    },
    bottomButton: {
        alignContent: 'center',
        borderTopColor: 'gray',
        borderTopWidth: 1,
        padding: 10,
        textAlign: 'center',
    }
})

export default Login