import React,{useEffect,useState,Component} from 'react'
import { Text, View,Button } from 'react-native'
import firebase from 'firebase/compat'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchUser,fetchUserPosts,fetchUserFollowing,clearData } from '../redux/actions/index'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import Feed from './main/feed/feed'
import Chat from './main/chat/chat'

import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import add from './main/add/add'
import profile from './main/profile/profile'
import Search from './main/profile/Search'
Search

const Tab= createMaterialBottomTabNavigator();

const EmptyScreen=()=>{
    return(null)
}

export class Main extends Component{


   componentDidMount(){
    this.props.clearData();
       this.props.fetchUser();
       this.props.fetchUserPosts();
       this.props.fetchUserFollowing();
   }
    //  const {currentUser}=props.fetchUser();
    //  console.log(props.fetchUser())

    render(){
        const {currentUser}=this.props;
        // console.log(currentUser) 


        if(currentUser==undefined)
        {
            return(
                <View>

                </View>
            )
        }
  return (
    <View style={{flex:1}}> 
            <Tab.Navigator initialRouteName='feed' labeled={false}>
            <Tab.Screen name="feed" component={Feed} 
            options={{
                tabBarIcon:({color,size})=>(
                    <MIcon color={color} size={26} name="home"/>
                )
            }}/>
            <Tab.Screen name="Search" component={Search} navigation={this.props.navigation}
                    options={{
                        tabBarIcon: ({ color, size }) => (
                            <MIcon name="magnify" color={color} size={26} />
                        ),
                    }} />
            <Tab.Screen name="Mainadd" component={EmptyScreen} 
            listeners={({navigation})=>({
                tabPress:event=>{
                    event.preventDefault();
                    navigation.navigate("add")
                }
            })}
            options={{
                tabBarIcon:({color,size})=>(
                    <MIcon color={color} size={26} name="plus-box"/>
                )
            }}/>
            <Tab.Screen name="profile" component={profile} 
            listeners={({ navigation }) => ({
                tabPress: event => {
                    event.preventDefault();
                    navigation.navigate("profile", {uid: firebase.auth().currentUser.uid})
                }})}
            options={{
                tabBarIcon:({color,size})=>(
                    <MIcon color={color} size={26} name="account-circle"/>
                )
            }}/>
            <Tab.Screen name="chat" component={Chat} options={{
                tabBarIcon:({color,size})=>(
                    <MIcon color={color} size={26} name="message"/>
                )
            }}/>
            </Tab.Navigator>
    </View>
    )
}
}

const mapStateToProps=(store)=>({
    currentUser:store.userState.currentUser
})

const mapDispatchProps = (dispatch)=>bindActionCreators({fetchUser,fetchUserPosts,fetchUserFollowing,clearData  },dispatch);

export default connect(mapStateToProps,mapDispatchProps)(Main);

// export default Main; 