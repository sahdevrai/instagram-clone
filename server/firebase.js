import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/compat/storage';



const firebaseConfig = {
    apiKey: "AIzaSyCFhlspvmQ-w1_xLx4P-OSm5ojyHfXthec",
    authDomain: "instagram-clone-3a29f.firebaseapp.com",
    projectId: "instagram-clone-3a29f",
    storageBucket: "instagram-clone-3a29f.appspot.com",
    messagingSenderId: "901563232338",
    appId: "1:901563232338:web:460bc638e74e16123eef8b",
    measurementId: "G-932RHN6K3Z"
  };
  
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  
  export default firebase; 