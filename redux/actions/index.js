import { USER_STATE_CHANGE, USER_POSTS_STATE_CHANGE, USER_FOLLOWING_STATE_CHANGE, USERS_DATA_STATE_CHANGE,USERS_POSTS_STATE_CHANGE, USERS_LIKES_STATE_CHANGE, CLEAR_DATA} from '../constants/index'
import firebase from 'firebase/compat'

export function clearData() {
    return ((dispatch) => {
        dispatch({type: CLEAR_DATA})
    })
}
export function fetchUser() {
    return ((dispatch) => {
        firebase.firestore()
            .collection("users")
            .doc(firebase.auth().currentUser.uid)
            .get()
            .then((snapshot) => {
                if (snapshot.exists) {
                    dispatch({ type: USER_STATE_CHANGE, currentUser: snapshot.data() })
                }
                else {
                    console.log('does not exist')
                }
            })
    })
}

export function fetchUserPosts() {
    return ((dispatch) => {
        firebase.firestore()
            .collection("posts")
            .doc(firebase.auth().currentUser.uid)
            .collection("userPosts")
            .orderBy("creation", "asc")
            .get()
            .then((snapshot) => {
                let posts = snapshot.docs.map(doc => {
                    const data = doc.data();
                    const id = doc.id;
                    return { id, ...data }
                })
                dispatch({ type: USER_POSTS_STATE_CHANGE, posts })
            })
    })
}

export function fetchUserFollowing() {
    return ((dispatch) => {
        firebase.firestore()
            .collection("following")
            .doc(firebase.auth().currentUser.uid)
            .collection("userFollowing")
            .onSnapshot((snapshot) => {
                let following = snapshot.docs.map(doc => {
                    const id = doc.id;
                    return id
                })
                dispatch({ type: USER_FOLLOWING_STATE_CHANGE, following });
                for(let i = 0; i < following.length; i++){
                    dispatch(fetchUsersData(following[i], true));
                }
            })
    })
}

export function fetchUsersData(uid, getPosts) {
    return ((dispatch, getState) => {
        const found = getState().usersState.users.some(el => el.uid === uid);
        if (!found) {
            firebase.firestore()
                .collection("users")
                .doc(uid)
                .get()
                .then((snapshot) => {
                    if (snapshot.exists) {
                        let user = snapshot.data();
                        user.uid = snapshot.id;

                        dispatch({ type: USERS_DATA_STATE_CHANGE, user });
                    }
                    else {
                        console.log('does not exist')
                    }
                })
                if(getPosts){
                    dispatch(fetchUsersFollowingPosts(uid));
                }
        }
    })
}

export function fetchUsersFollowingPosts(uid) {
    return ((dispatch, getState) => {
const arr=[]
        // firebase.firestore()
        //     .collection("posts")
        //     .doc(uid)
        //     .collection("userStory")
        //     .orderBy("creation", "asc")
        //     .get()  
        //     .then((secondsnapshot) => {
        //         console.log("--------story---------")
        //         console.log(secondsnapshot)
        //     })

        firebase.firestore()
            .collection("posts")
            .doc(uid)
            .collection("userPosts")
            .orderBy("creation", "asc")
            .get()
            .then((snapshot) => {
                console.log('snapfetch')
                console.log(snapshot)
                console.log("snapshotqwertyuiop")
                if(snapshot.docs.length===0)
                {
                    return
                }
               // console.log('---------------------------return----------------------')
               // console.log(snapshot.docs['0'].ref.path.split('/')[3])

              //  console.log(snapshot.docs.ref)

                const uid = snapshot.docs['0'].ref.path.split('/')[1];
                const user = getState().usersState.users.find(el => el.uid === uid);
              //  console.log('user')
                //console.log(user)

                let posts = snapshot.docs.map(doc => {
                    const data = doc.data();
                    const id = doc.id;
                    return { id, ...data, user }
                })
              //  console.log('poswewerts')
                //console.log(posts)

                for(let i = 0; i< posts.length; i++){
                    dispatch(fetchUsersFollowingLikes(uid, posts[i].id))
                }
                dispatch({ type: USERS_POSTS_STATE_CHANGE, posts, uid })

            })
    })
}

export function fetchUsersFollowingLikes(uid, postId) {
    return ((dispatch, getState) => {
const arr=[]
        // firebase.firestore()
        //     .collection("posts")
        //     .doc(uid)
        //     .collection("userPosts")
        //     .doc(postId)
        //     .collection("likes")
        //     .where("likes", "==", true)
        //     .get()
        //     .then((querySnapshot)=>{
        //         console.log('----------------log----------------')
        //         querySnapshot.forEach((doc) => {
        //             console.log('----------------log----------------')
        //             // doc.data() is never undefined for query doc snapshots
        //             console.log(doc.id, " => ", doc.data());
        //             arr.push(doc.id)
        //             console.log('---------------------like-----------------')
        //             console.log(arr)
        //         });

        //     })
            

        firebase.firestore()
            .collection("posts")
            .doc(uid)
            .collection("userPosts")
            .doc(postId)
            .collection("likes")
            .doc(firebase.auth().currentUser.uid)
            .onSnapshot((snapshot) => {
                // console.log("snapshot")
                // console.log(snapshot)
                // console.log(snapshot.ref.path.split('/')[3])
                // console.log(snapshot.docs)
                const postId = snapshot.ref.path.split('/')[3];

                        
                let currentUserLike = false;
                if(snapshot.exists){
                    currentUserLike = true;
                }

                dispatch({ type: USERS_LIKES_STATE_CHANGE, postId, currentUserLike })
            })
    })
}
// export function fetchAllLikes(uid, postId) {
//     return ((dispatch, getState) => {

//         firebase.firestore()
//             .collection("posts")
//             .doc(uid)
//             .collection("userPosts")
//             .doc(postId)
//             .collection("likes")
//             .doc(firebase.auth().currentUser.uid)
//             .onSnapshot((snapshot) => {
//                 console.log("snapshot")
//                 console.log(snapshot)
//                 console.log(snapshot.ref.path.split('/')[3])
//                 console.log(snapshot.docs)
//                 const postId = snapshot.ref.path.split('/')[3];

                        
//                 let currentUserLike = false;
//                 if(snapshot.exists){
//                     currentUserLike = true;
//                 }

//                 dispatch({ type: USERS_All_LIKES_STATE_CHANGE, postId, currentUserLike })
//             })
//     })
// }